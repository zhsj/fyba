fyba (4.1.1-10) unstable; urgency=medium

  * Team upload.
  * Add dpkg-dev (>= 1.22.5) to build dependencies for t64 changes.
    (closes: #1065257)

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 02 Mar 2024 11:50:57 +0100

fyba (4.1.1-9) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.
    (closes: #1061961)

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 28 Feb 2024 14:40:18 +0100

fyba (4.1.1-9~exp1) experimental; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Enable Salsa CI.
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Use not-installed file instead of dh_install override.
  * Drop obsolete -Wl,--as-needed linker flag, used by default.

  [ Lukas Märdian ]
  * Rename libraries for 64-bit time_t transition.
    (closes: #1061961)

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 30 Jan 2024 15:27:24 +0100

fyba (4.1.1-8) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.1, no changes.
  * Update watch file for GitHub URL changes.
  * Bump debhelper compat to 12, changes:
    - Drop -V from dh_makeshlibs
  * Explicitly remove files that are not installed.
  * Drop obsolete dh_strip override, dbgsym migration complete.
  * Add Rules-Requires-Root to control file.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 01 Dec 2022 09:22:00 +0100

fyba (4.1.1-7) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.5.0, no changes.
  * Update watch file to limit matches to archive path.
  * Update gbp.conf to use --source-only-changes by default.
  * Drop Name field from upstream metadata.
  * Bump debhelper compat to 10, changes:
    - Don't explicitly enable autoreconf, enabled by default
    - Drop dh-autoreconf build dependency
  * Bump watch file version to 4.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 11 Nov 2020 17:32:29 +0100

fyba (4.1.1-6) unstable; urgency=medium

  * Add a sensible autopkgtest which verifies that it is possible to
    link with libfyba.
    - Added these files:
        debian/tests/control
        debian/tests/link-to-library
  * Remove lintian override for testsuite-autopkgtest-missing.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 02 Aug 2018 20:28:34 +0200

fyba (4.1.1-5) unstable; urgency=medium

  * Team upload.
  * Drop autopkgtest to test installability.
    (closes: #905117)
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 31 Jul 2018 14:13:20 +0200

fyba (4.1.1-4) unstable; urgency=medium

  * Team upload.
  * Change Maintainer to Debian GIS project, move Ruben to Uploaders.
  * Restructure control file with cme.
  * Drop autoconf & libtool build dependencies, pulled in via dh-autoreconf.
  * Update watch file to handle common issues.
  * Drop incorrect Forwarded header from patches.
  * Add upstream metadata.
  * Bump Standards-Version to 4.1.5, no changes.
  * Strip trailing whitespace from control file.
  * Update copyright-format URL to use HTTPS.
  * Update Vcs-* URLs for Salsa.
  * Don't use autotools-dev explicitly.
  * Drop obsolete dbg package.
  * Add autopkgtest to test installability.
  * Enable all hardening buildflags.
  * Don't limit dh_strip to libfyba0.
  * Override dh_makeshlibs to use -V.
  * Fix capitalization error in description synopsis.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 29 Jul 2018 21:10:50 +0200

fyba (4.1.1-3) unstable; urgency=medium

  * debian/control:
    - Set Vcs URLs to https
    - Updated standards to 3.9.8 (no changes)
  * debian/patches/fix_issue.patch
    - Don't define min/max macros
      Fixes FTBFS with GCC 6 (Closes: #831206)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 19 Aug 2016 17:59:12 +0000

fyba (4.1.1-2) unstable; urgency=low

  * debian/control:
    - Fixed debhelper version 9.0.0 -> 9
    - Set "Multi-Arch: same" to enable multiarch for library
    - Standards version to 3.9.6
  * Added debian/gbp.conf to automatically enable pristine-tar build

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 23 May 2015 12:20:40 +0200

fyba (4.1.1-1) unstable; urgency=low

  * Initial release (Closes: #760544)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 04 Sep 2014 21:41:19 +0200
